import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    companyData: null
  },

  mutations: {
    setCompanyData(state, data) {
      state.companyData = data;
    }
  },

  actions: {
    async fetchCompanyData({ commit }) {
      try {
        const response = await fetch('https://bitbucket.org/ilakhmotkin/front-end-assesment-ru/raw/525e8e2fbee5cd98a80499c400f569e77b4f6ffd/server-response.json');
        const data = await response.json();
        commit('setCompanyData', data);
      } catch (error) {
        console.error(error);
      }
    }
  },

  getters: {
    companyData: s => s.companyData
  }
})


export default store;